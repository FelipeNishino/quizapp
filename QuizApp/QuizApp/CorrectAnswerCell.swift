//
//  CorrectAnswerCell.swift
//  QuizApp
//
//  Created by Nishi on 04/08/23.
//

import UIKit

class CorrectAnswerCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answerLabel: UILabel!
}
