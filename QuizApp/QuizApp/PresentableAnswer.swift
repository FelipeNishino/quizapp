//
//  PresentableAnswer.swift
//  QuizApp
//
//  Created by Nishi on 04/08/23.
//

struct PresentableAnswer {
    let question: String
    let answer: String
    let wrongAnswer: String?
}
