//
//  QuestionViewController.swift
//  QuizApp
//
//  Created by Nishi on 07/07/23.
//

import Foundation
import UIKit

class QuestionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private var question = ""
    private var options = [String]()
    private var selection: (([String]) -> Void)? = nil
    private let reuseIdentifier = "Cell"
    
    convenience init(question: String, options: [String], selection: @escaping ([String]) -> Void) {
        self.init()
        self.question = question
        self.options = options
        self.selection = selection
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerLabel.text = question
        headerLabel.textAlignment = .center
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dequeueCell(int: tableView)
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selection?(selectedOptions(in: tableView))
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.allowsMultipleSelection {
            selection?(selectedOptions(in: tableView))
        }
    }
    
    private func selectedOptions(in tableView: UITableView) -> [String] {
        guard let indexPaths = tableView.indexPathsForSelectedRows else { return [] }
        return indexPaths.map{ options[$0.row] }
    }
    
    private func dequeueCell(int tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) {
            return cell
        }
        return UITableViewCell(style: .default, reuseIdentifier: reuseIdentifier)
    }
}

//        override func loadView() {
//            super.loadView()
//            let headerLabel = UILabel(frame: .zero)
//            headerLabel.translatesAutoresizingMaskIntoConstraints = false
//            let tableView = UITableView(frame: .zero)
//            tableView.translatesAutoresizingMaskIntoConstraints = false
//            view.addSubview(tableView)
//            tableView.tableHeaderView = headerLabel
//
//            NSLayoutConstraint.activate([
//                tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
//                tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
//                tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
//                tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
//                headerLabel.heightAnchor.constraint(equalToConstant: 44),
//    //            headerLabel.widthAnchor.constraint(equalTo: tableView.widthAnchor),
//            ])
//            // Setting frame manually as a workaround described in https://stackoverflow.com/a/76540476
//            headerLabel.frame.size =  headerLabel.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
//
//            self.headerLabel = headerLabel
//            self.tableView = tableView
//        }
