//
//  WrongAnswerCell.swift
//  QuizApp
//
//  Created by Nishi on 04/08/23.
//

import UIKit

class WrongAnswerCell: UITableViewCell {
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var correctAnswerLabel: UILabel!
    @IBOutlet weak var wrongAnswerLabel: UILabel!
}
