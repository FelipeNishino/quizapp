//
//  Created by Nishi on 04/08/23.
//

import Foundation
import QuizEngine

// Spy: An helper class in a testing environment for collecting analytics about the SUT as a way to ensure desired behaviours
class RouterSpy: Router {
    var routedQuestions: [String] = []
    var routedResult: Result<String, String>? = nil
    
    var answerCallback: ((String) -> Void) = { _ in }
    
    func routeTo(question: String, answerCallback: @escaping (String) -> Void) {
        routedQuestions.append(question)
        self.answerCallback = answerCallback
    }
    
    func routeTo(result: Result<String, String>) {
        routedResult = result
    }
}
