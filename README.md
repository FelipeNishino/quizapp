# QuizApp - a code-along

## Description
This project is a simple quiz app, the objective is to learn to create a modular and clean codebase, using the TDD (Test-Driven Development) methodology.
The code-along is based on the youtube series from the channel Essential Developer: [Building iOS Apps with Swift, TDD & Clean Architecture | Professional iOS Engineering Series](https://www.youtube.com/playlist?list=PLyjgjmI1UzlSUlaQD0RvLwwW-LSlJn-F6)

